
import os

f = os.popen("sudo iwlist wlan0 scan")

now = f.read()

for data in now.split("Cell"):
    essid = ""
    quality = 0
    frequency = 0
    for i in data.split('\n'):
        if "ESSID:" in i:
            essid = i.split('ESSID:')[-1][1:-1]
        if "Quality=" in i:
            quality = int(i.split('Quality=')[-1][:2]) * 100 / 70
        if "Frequency:" in i:
            frequency = int(i.split("Frequency:")[-1][:1])
            
        if len(essid) > 0:
            cell = int(data[:4])
            out = {
                'id': cell,
                'essid': essid,
                'quality': int(quality),
                'frequency': frequency,
            }
            print(out)
            #print(cell, essid, "{:.1f}%".format(quality), frequency)
            break
