from django.shortcuts import render, HttpResponse
import json
import os, sys, subprocess
from django.views.decorators.csrf import csrf_exempt
import time
import urllib.request as requests


def signalLevel(val):
    return val['quality']

def find_in_list(lst, key, value):
    for i, dic in enumerate(lst):
        if dic[key] == value:
            return i
    return -1

@csrf_exempt
def scan(request):
    os.system("sudo ifconfig wlan0 up")

    networks = []
    f = os.popen("sudo iwlist wlan0 scan")

    now = f.read()

    for data in now.split("Cell"):
        #print(data.split('\n'))
        essid = ""
        quality = 0
        frequency = 0
        security = ""
        for i in data.split('\n'):
            if "ESSID:" in i:
                essid = i.split('ESSID:')[-1][1:-1]

            # La salida del comando para quality cambia en la pi
            try:
                if "Signal level=" in i:
                    quality = int(i.split('Signal level=')[-1].split("/")[0])
            except Exception:
                try:
                    if "Quality=" in i:
                        quality = int(i.split('Quality=')[-1][:2]) * 100 / 70
                except Exception:
                    quality = 100
            
            if "Frequency:" in i:
                frequency = int(i.split("Frequency:")[-1][:1])

            if "Encryption key:" in i:
                security = i.split("Encryption key:")[-1]

        if len(essid) > 0:
            cell = int(data[:4])
            out = {
                'id': cell,
                'essid': essid,
                'quality': int(quality),
                'frequency': frequency,
                'security': security
            }
            
            idx = find_in_list(networks, 'essid', out['essid'])
            if idx != -1:
                if networks[idx]['quality'] < out['quality']:
                    networks.pop(idx)

            networks.append(out)
    
    networks.sort(key=signalLevel, reverse=True)
    return HttpResponse(json.dumps(networks), content_type='application/json')

@csrf_exempt
def connect(request):
    os.system("sudo ifconfig wlan0 up")
    data = json.loads(request.body.decode('utf8'))

    # remueve logs y claves existentes. mato conexiones existentes
    #os.system('rm -f out_key')
    os.system('rm -f log_wpa')
    estado = os.system('sudo pkill wpa_supplicant')
    print("- ESTADO WPA_SUPPLICANT:", estado)
    
    time.sleep(1)

    # crea una nueva clave para la red wifi
    estado = os.system('sudo printf "update_config=1\nctrl_interface=DIR=/var/run/wpa_supplicant GROUP=sudo\n" > out_key')
    print("- ESTADO PRINT HEADERS CONF:", estado)

    if data['password'] != '':
        estado = os.system('sudo wpa_passphrase "' + data['ssid'] + '" "' + data['password'] + '" >> out_key')
    else:
        f = open('out_key', 'a')
        f.write("network={\nssid=\"" + data['ssid'] +"\"\nkey_mgmt=NONE\n}")
        f.close()
    print("- ESTADO GENERACION PASSPHRASE:", estado)

    
    # appendea lineas de configuracion a la clave para
    # poder chequear estado de conexion

    # conecta
    os.popen("sudo wpa_supplicant -Dwext -iwlan0 -B -f log_wpa -c out_key")

    status = ''
    handshake = 0
    
    # verifico estado
    #
    time.sleep(1)

    for _ in range(60*5):
        f = os.popen("sudo wpa_cli status")
        for linea in f.readlines():
            if "wpa_state" in linea:
                linea = linea.strip()
                status = linea.split("=")[1]
                break

        if status != '':
            if status == "4WAY_HANDSHAKE":
                handshake = 1
            if status == "COMPLETED":
                break
        
        time.sleep(0.2)

    print("- ESTADO DE CONEXION:",status)

    if status == 'COMPLETED':
        estado = os.system("sudo dhclient -x wlan0")
        estado = os.system("sudo dhclient wlan0")
        estado = os.system("echo 'echo nameserver 8.8.8.8 > /etc/resolv.conf' | sudo bash")
        return HttpResponse(json.dumps({"status":"ok"}), content_type='application/json')
    elif status == 'SCANNING':
        if handshake:
            return HttpResponse(json.dumps({"status":"err_auth"}), content_type='application/json')
        else:
            return HttpResponse(json.dumps({"status":"err"}), content_type='application/json')
    else:
        return HttpResponse(json.dumps({"status":"err"}), content_type='application/json')



   
    
@csrf_exempt
def update_step1(request):
    estado = os.system("git checkout -- .")
    f = os.popen("git pull")
    lineas = f.readlines()

    if len(lineas) > 1:
        return HttpResponse(json.dumps({"status":"reboot_needed"}), content_type='application/json')
    else:
        return HttpResponse(json.dumps({"status":"ok"}), content_type='application/json')

@csrf_exempt
def reboot(request):
    if request.GET.get('clave') == 'QMICR9FX':
        print("vamo a rebootear")
        os.system("sudo reboot")
    else:
        print("buen intento")
        return HttpResponse(json.dumps({"status":"nice try"}), content_type='application/json')    

@csrf_exempt
def ping(request):
    return HttpResponse(json.dumps({"status":"ok"}), content_type='application/json')


@csrf_exempt
def check_finished(request):
    f = open('conf.json', 'r')

    conf = json.loads(f.read())
    path_app = conf['path_app_um']

    if os.path.isfile(f"{path_app}um_server_react/finish"):
        return HttpResponse(json.dumps({"status":"ok"}), content_type='application/json')
    else:
        return HttpResponse(json.dumps({"status":"no"}), content_type='application/json')

@csrf_exempt
def update_step2(request):
    f = open('conf.json', 'r')

    conf = json.loads(f.read())
    path_app = conf['path_app_um']
    branch = conf['branch']
    
    #test

    estado = os.system(f"sudo rm -rf {path_app}um_server_react/")
    estado = os.system(f"git clone https://gitlab.com/afranceschi/um_server_react.git --single-branch --branch {branch} {path_app}um_server_react/")

    estado = os.system(f"bash {path_app}um_server_react/config.sh")

    return HttpResponse(json.dumps({"status":"ok"}), content_type='application/json')




@csrf_exempt
def check_token(request):
    url = "https://backend.sipum.drimit.net/checkToken?token=" + request.GET.get('token')
    response = requests.urlopen(url)
    data = json.loads(response.read())
    
    return HttpResponse(json.dumps(data), content_type='application/json')

@csrf_exempt
def get_app_data(request):
    # pegarle a la app para que baje la data de la sede con el token que me pasaron
    # devolver lo que devuelva la app
    f = open('conf.json', 'r')

    conf = json.loads(f.read())
    path_app = conf['path_app_um']

    data = json.loads(request.body)
    url = "http://10.0.0.5:3001/recinto/getAppData/?token=" + data['token']
    print(url)

    response = requests.urlopen(url)
    response_data = response.read()
    data = json.loads(response_data)
    estado = os.system(f"bash {path_app}um_server_react/start_after_sync.sh")
    
    return HttpResponse(json.dumps(data), content_type='application/json')
