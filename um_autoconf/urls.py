"""um_autoconf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from backend import views as backend

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^scan/', backend.scan),
    url(r'^connect/', backend.connect),
    url(r'^updateStep1/', backend.update_step1),
    url(r'^updateStep2/', backend.update_step2),
    url(r'^reboot/', backend.reboot),
    url(r'^ping/', backend.ping),
    url(r'^checkFinish/', backend.check_finished),
    url(r'^checkToken/', backend.check_token),
    url(r'^getAppData/', backend.get_app_data),
]
