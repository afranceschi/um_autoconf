#!/bin/bash

export UM_RUTA=/home/pi/apps/um_server_react
export UM_SERVER_RUTA=/home/pi/apps/um_server_react/python-server

echo "ELIMINANDO CARPETA"
sudo rm -rf $UM_RUTA

echo "CLONANDO...."
git clone git@gitlab.com:afranceschi/um_server_react.git --single-branch --branch dev $UM_RUTA

echo "INSTALANDO...."
sh $UM_RUTA/config.sh
