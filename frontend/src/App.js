import React from 'react'
import { Typography } from '@material-ui/core'

import { withStyles } from '@material-ui/core/styles'
import './App.css'

import Sidebar from './components/Sidebar'
import WifiList from './components/WifiList'
import Installer from './components/Installer'
import Finisher from './components/Finisher'

const styles = theme => ({
	app: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'stretch',
		width: '100%',
		height: '100vh',
		margin: 0,
		padding: 0
	},
	main: {
		flex: 2,
		height: 'calc(100% - 50px - 20px)',
		display: 'flex',
		backgroundImage: `url(${process.env.PUBLIC_URL + '/statics/img/Mask_BG.png'})`
	},
	dialogMain: {
		flex: 1,
		//margin: '5vh',
		marginTop: '5vh',
		marginBottom: '15vh',
		marginLeft: '20%',
		marginRight: '20%',
		Height: '25vh',
		display: 'flex',
		flexDirection: 'column'
		//background: 'green',
	},
	dialogTitle: {
		flex: 1,
		display: 'flex',
		maxHeight: 50,
		//justifyContent: 'center',
		alignItems: 'center'
	},
	dialogContent: {
		flex: 1,
		display: 'flex',
		flexDirection: 'row',
		background: 'red'
	},
	Sidebar: {
		display: 'flex',
		flexDirection: 'column',
		flex: 1,
		maxWidth: 250,
		height: '100%',
		background: '#AC0038'
	},
	Content: {
		display: 'flex',
		flex: 1,
		background: '#F7F7F7'
	},

	topBar: {
		display: 'flex',
		alignItems: 'stretch',
		flex: 1,
		width: '100%',
		maxHeight: 50,
		minHeight: 50
	},
	lowBar: {
		display: 'flex',
		alignItems: 'stretch',
		flex: 1,
		width: '100%',
		marginBot: 0,
		maxHeight: 20,
		minHeight: 20
	}
})

class App extends React.Component {
	state = {
		screen: 0,
		wifiName: '',
		wifiPass: '',
	}

	nextScreen = () => {
		if (this.state.screen < 2) this.setState({ screen: this.state.screen + 1 })
	}

	prevScreen = () => {
		if (this.state.screen > 0) {
			this.setState({ screen: this.state.screen - 1 })
		}
	}

	finish = (wifiName, wifiPass) => {
		if (this.state.screen === 0) this.setState({ wifiName, wifiPass })
		// else if (this.state.screen === 1) this.setState({})
		this.nextScreen()
	}

	restartInstaller = ()=> {
		window.location.reload()
	}

	render() {
		const { classes } = this.props
		const { screen } = this.state
		return (
			<div className={classes.app}>
				<div>
					<img
						className={classes.topBar}
						alt="TopBar"
						src={process.env.PUBLIC_URL + '/statics/img/topbar.png'}
					/>
				</div>
				<div className={classes.main}>
					<div className={classes.dialogMain}>
						<div className={classes.dialogTitle}>
							<Typography style={{ color: '#707070' }} variant="h5" component="h5">
								ASISTENTE DE INSTALACION
							</Typography>
						</div>
						<div className={classes.dialogContent}>
							<div className={classes.Sidebar}>
								<Sidebar screen={screen} />
							</div>
							<div className={classes.Content}>
								{screen === 0 ? (
									<WifiList finish={this.finish} />
								) : screen === 1 ? (
									<Installer restartInstaller={this.restartInstaller} finish={this.finish} />
								) : screen === 2 ? (
									<Finisher finish={this.finish} />
								) : null}
							</div>
						</div>
					</div>
				</div>
				<div>
					<img
						className={classes.lowBar}
						alt="BotBar"
						src={process.env.PUBLIC_URL + '/statics/img/lowbar.png'}
					/>
				</div>
			</div>
		)
	}
}

export default withStyles(styles)(App)
