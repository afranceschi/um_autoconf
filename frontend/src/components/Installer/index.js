import React from 'react'
import { withStyles, Typography, Paper, LinearProgress } from '@material-ui/core'
import { Check } from '@material-ui/icons'
import Fade from 'react-reveal/Fade'

import { apiGet, apiGetWithParams } from '../../api'
import { urlUpdateStep1, urlUpdateStep2, urlReboot, urlUp, urlCheckFinish } from '../../constants'

const styles = theme => ({
	root: {
		flex: 1,
		display: 'flex',
		flexDirection: 'column',
		padding: 30
	},
	paper: {
		//width: '100%',
		padding: '5%',
		margin: '20px 0',
		display: 'flex'
	},
	okIcon: {
		color: '#73a950',
		marginLeft: 10
	}
})

const ColorLinearProgress = withStyles({
	colorSecondary: {
		backgroundColor: '#b2dfdb'
	},
	barColorSecondary: {
		backgroundColor: '#73a950'
	}
})(LinearProgress)

class Installer extends React.Component {
	state = {
		title: 'Actualizacion automatica',
		currentAction: 'Buscando actualizaciones...',
		complete: false
	}

	componentDidMount() {
		// 
		// setTimeout(
		// 	() =>
		// 		apiGet(urlCheckUpdate)
		// 			.then(r => {
		// 				if (r.status === 'err') alert('Error al obtener información de actualizaciones')
		// 				else if (r.status === 'si') {
		// 					this.setState({ complete: true, currentAction: 'Nueva version encontrada' })
		// 					setTimeout(this.download, 3000)
		// 				} else if (r.status === 'no') {
		// 					this.setState({ complete: true, currentAction: 'El sistema esta actualizado' })
		// 					setTimeout(this.props.finish, 6000)
		// 				} else alert('Error al obtener información de actualizaciones')
		// 			})
		// 			.catch(r => alert('Error en el servidor')),
		// 	3000
		// )
		this.step1()
	}

	step1 = () => {
		this.setState({
			currentAction: 'Actualizando instalador...',
			complete: false
		})
		apiGet(urlUpdateStep1)
			.then(r => {
				if (r.status === 'err') alert('Error al obtener actualizacion')
				else if (r.status === 'reboot_needed') {
					this.setState({ complete: true, currentAction: 'Nueva version instalada, reiniciando...' })
					apiGetWithParams(urlReboot, {clave: 'QMICR9FX'}).catch(r => console.log('Reiniciando...'))
					const id = setInterval(
						() =>
							apiGet(urlUp).then(r => {
								if (r.status === 'ok') {
									clearInterval(this.state.idInterval)
									this.props.restartInstaller()
								}
							}),
						11000
					)
					this.setState({ idInterval: id })
				} else if (r.status === 'ok') {
					this.setState({ complete: true, currentAction: 'El instalador esta actualizado' })
					setTimeout(this.step2, 5000)
				}
			})
			.catch(r => alert('Error en el servidor'))
	}

	step2 = () => {
		apiGet(urlUpdateStep2).catch(r => console.log('Instalando actualizacion...'))
		const idInterval = setInterval(
			() => apiGet(urlCheckFinish)
				.then(r => {
					if (r.status === 'ok') {
						clearInterval(this.state.idInterval)
						setTimeout(
							() => this.setState({ complete: true, currentAction: 'Instalacion completada' }),
							1200
						)
						setTimeout(this.props.finish, 6000)
					}
				})
				.catch(r => alert('Error en el servidor')),
			10000
		)
		this.setState({
			currentAction: 'Instalando App...',
			idInterval,
			complete: false
		})
	}

	render() {
		const { classes } = this.props
		const { title, currentAction, complete } = this.state
		return (
			<div className={classes.root}>
				<div className={classes.title}>
					<Typography style={{ color: '#707070' }} variant="h6" component="h6">
						{title}
					</Typography>
					<Typography style={{ color: '#707070' }}>Espera un momento, esto podría tardar varios minutos</Typography>
				</div>
				<Paper className={classes.paper}>
					<Typography style={{ color: '#707070' }}>{currentAction}</Typography>
					<Fade right when={complete}>
						<Check className={classes.okIcon} />
					</Fade>
				</Paper>
				<ColorLinearProgress
					color={complete ? 'secondary' : 'primary'}
					variant={complete ? 'determinate' : 'indeterminate'}
					value={complete ? 100 : undefined}
				/>
			</div>
		)
	}
}

export default withStyles(styles)(Installer)
