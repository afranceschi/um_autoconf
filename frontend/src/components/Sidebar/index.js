import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    List,
    ListItem,
    Typography,
} from '@material-ui/core';

const sidebarClasses = makeStyles(theme => ({
    root:{
        display: 'flex', 
        flexDirection:'column',
        flex:1,
    },
    logo: {
      display: 'flex',
      flex: 1,
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    logo_img:{
      maxWidth: 130,
      minWidth: 130,
    },
    menu: {
      flex: 1,
    },
    item_selected: {
      //background: '#8f0',
      background: '#860E35',
    },
    item: {
      justifyContent: 'center',
      height: 50,
      //background: 'blue',
    },
  }));

function Sidebar(props){

    const sidebar_classes = sidebarClasses();
    const {screen} = props

    return(
        <div className={sidebar_classes.root}>
            <div className={sidebar_classes.logo}>
                <img className={sidebar_classes.logo_img} alt="BackgroundImage" src={process.env.PUBLIC_URL + '/statics/img/UM-Logo.png'} />
            </div>
            <div className={sidebar_classes.menu}> 
            <List>
                <ListItem className={screen === 0 ? [sidebar_classes.item, sidebar_classes.item_selected] : sidebar_classes.item}>
                <Typography style={{color: 'white'}}>Preparando</Typography>
                </ListItem>
                <ListItem className={screen === 1 ? [sidebar_classes.item, sidebar_classes.item_selected] : [sidebar_classes.item]}>
                <Typography style={{color: 'white'}}>Instalando</Typography>
                </ListItem>
                <ListItem className={screen === 2 ? [sidebar_classes.item, sidebar_classes.item_selected] : [sidebar_classes.item]}>
                <Typography style={{color: 'white'}}>Finalizando</Typography>
                </ListItem>
            </List>
            </div> 
        </div>
    )
}

export default Sidebar;