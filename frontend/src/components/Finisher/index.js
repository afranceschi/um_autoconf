import React from 'react'
import { withStyles, Typography, TextField, Button, CircularProgress } from '@material-ui/core'
import Fade from 'react-reveal/Fade'
import { apiGetWithParams, apiPost } from '../../api'
import { urlCheckCode, urlSendCode } from '../../constants'

const styles = theme => ({
	root: {
		flex: 1,
		display: 'flex',
		flexDirection: 'column',
		padding: 30,
		justifyContent: 'space-between'
	},
	title: {
		padingBottom: 20
	},
	textField: {
		borderRadius: 10, 
	},
	textfieldDiv: {
		padding: '15px 0',
	},
	okButton: {
		backgroundColor: '#73a950',
		borderRadius: 30,
		textTransform: 'none',
		boxShadow: 'none',
		'&:hover': {
			backgroundColor: '#637930'
		},
		width: 150,
		marginLeft: '10px'
	},
	buttonDiv: {
		alignSelf: 'flex-end',
		flexDirection: 'row',
		display: 'flex',
		alignItems:'center'
	},
	finishDiv: {
		flex: 1,
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column'
	}
})

class Finisher extends React.Component {
	state = {
		code: '',
		sede: '',
		screen: 0,
		idTimeout: -1,
		waiting: false,
	}

	nextScreen = () => {
		this.setState({ screen: 1 })
	}

	checkCode = token => {
		apiGetWithParams(urlCheckCode, { token }).then(r => {
			if (r.status.toLowerCase() === 'ok') this.setState({ sede: r.data })
		}).catch(r => alert('error en el servidor'))
	}

	handleChange = e => {
		this.setState({ code: e.target.value })
		const code = e.target.value
		clearTimeout(this.state.idTimeout)
		if (code) {
			const idTimeout = setTimeout(() => this.checkCode(code), 500)
			this.setState({ idTimeout })
		}
	}

	sendCode = () => {
		this.setState({waiting: true})
		apiPost(urlSendCode, { token: this.state.code })
			.then(r => {
				if (r.status.toLowerCase() === 'ok') {
					this.nextScreen()
				} else alert('Error al conformar la sede')
				this.setState({waiting: false})
			})
			.catch(r => {
				alert('Error en el servidor, intente de nuevo')
				this.setState({waiting: false})
			})
	}

	render() {
		const { classes } = this.props
		const { sede, code, screen, waiting } = this.state
		return (
			<div className={classes.root}>
				{screen === 0 ? (
					<React.Fragment>
						<div>
							<div className={classes.title}>
								<Typography style={{ color: '#707070' }} variant="h4">
									Ingresar número del modelo
								</Typography>
							</div>
							<div className={classes.textfieldDiv}>
								<TextField
									id={'code'}
									name={'code'}
									key={'code'}
									placeholder={'Codigo'}
									className={classes.textField}
									value={code}
									onChange={this.handleChange}
									margin="normal"
									variant="filled"
									fullWidth
									InputProps={{
										autoComplete: 'off',
										style: {
											width: '100%',
											height: '100%',
											borderRadius: 10
										},
										disableUnderline: true,
									}}
									inputProps={{
										style: {
											backgroundColor: 'white',
											color: '#707070',
											fontSize: '30px',
											border: '1.5px solid #eee',
											borderRadius: 10,
											padding: '20px 40px'
										}
									}}
									disabled={waiting}
								/>
							</div>
							<div className={classes.sede}>
								<Typography style={{ color: '#707070' }} variant="h6" component="h6">
									{sede} 
								</Typography>
							</div>
						</div>
						<div className={classes.buttonDiv}>
							<Fade right when={sede}>
								{waiting? <CircularProgress size='25px'/> : <React.Fragment/>}
								<Button
									className={classes.okButton}
									variant="contained"
									color="primary"
									onClick={this.sendCode}
									disabled={!sede || waiting}
								>
									Siguiente
								</Button>
							</Fade>
						</div>
					</React.Fragment>
				) : screen === 1 ? (
					<div className={classes.finishDiv}>
						<Typography style={{ paddingBottom: 20, color: '#707070' }} variant="h3">
							¡ Todo listo !
						</Typography>
						<Typography style={{ paddingBottom: 20, color: '#707070' }} variant="h6">
							Ya podés comenzar a utilizar el sistema
						</Typography>
						<Button
							className={classes.okButton}
							variant="contained"
							color="primary"
							onClick={() => {
								window.location = 'http://app.um/'
							}}
						>
							Finalizar
						</Button>
					</div>
				) : null}
			</div>
		)
	}
}

export default withStyles(styles)(Finisher)
