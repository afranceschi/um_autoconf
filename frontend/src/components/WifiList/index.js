import React from 'react'
import {
	List,
	Paper,
	Typography,
	TextField,
	Dialog,
	DialogTitle,
	DialogContent,
	DialogActions,
	Button,
	ListItem,
	ListItemText,
	InputAdornment,
	IconButton,
	CircularProgress
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import {
	SignalWifi4Bar,
	Lock,
	Visibility,
	VisibilityOff,
	ArrowBack,
	SignalWifi0Bar,
	SignalWifi1Bar,
	SignalWifi2Bar,
	SignalWifi3Bar
} from '@material-ui/icons'
import Fade from 'react-reveal/Fade'

import { apiGet, apiPost } from '../../api/index'
import { urlScan, urlConnect } from '../../constants'

const styles = theme => ({
	root: {
		flex: 1,
		display: 'flex',
		flexDirection: 'column',
		padding: 30
		//background: 'blue',
	},
	content: {
		flex: 3,
		display: 'flex'
	},
	paper: {
		flex: 1,
		overflowX: 'hidden',
		overflowY: 'auto',
		height: '100%'
		//maxHeight: '35vh'
	},
	rootItem: {
		flex: 1,
		display: 'flex',
		flexDirection: 'column'
	},
	primaryLineItem: {
		flex: 1,
		display: 'flex',
		flexDirection: 'row'
	},
	secondLineItem: {
		flex: 1,
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-end'
	},
	iconsItem: {
		fontSize: 18,
		marginLeft: 10
	},
	paperItem: {
		position: 'absolute',
		width: 400,
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(4),
		outline: 'none'
	},
	actions: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
	connButton: {
		borderRadius: 90,
		width: '40%',
		backgroundColor: '#3373ac',
		textTransform: 'none',
		boxShadow: 'none',
		'&:hover': {
			boxShadow: 'none',
			backgroundColor: '#23639c'
		}
	},
	backButton: {
		color: '#3373ac'
	},
	dialogTitle: {
		color: '#707070'
	},
	waitingPaper: {
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	textField: {
		borderRadius: 30
	},
	manualButton:{
		width: '50%',
		textTransform: 'none',
		alignSelf: 'center',
		marginTop: '10px',
		borderRadius: 90,
		boxShadow: 'none',
		'&:hover': {
			boxShadow: 'none'
		}
	}
})

class WifiList extends React.Component {
	state = {
		screen: 0,
		wifiName: '',
		wifiPass: '',
		open: false,
		showPass: false,
		wifiList: [],
		security: 'on',
		idInterval: -1,
		ssidManual: false
	}

	componentDidMount() {
		this.scan()
	}

	scan = () => {
		apiGet(urlScan)
			.then(r => {
				this.setState({ wifiList: r })
			})
			.catch(r => alert('Error al obtener lista de WiFi'))
		const id = setInterval(
			() =>
				apiGet(urlScan)
					.then(r => {
						this.setState({ wifiList: r })
					})
					.catch(r => alert('Error al obtener lista de WiFi')),
			30000
		)
		this.setState({ idInterval: id })
	}

	nextScreen = () => {
		if (this.state.screen === 0) clearInterval(this.state.idInterval)
		if (this.state.screen < 2) this.setState({ screen: this.state.screen + 1 })
	}

	prevScreen = () => {
		if (this.state.screen === 1) this.scan()
		if (this.state.screen > 0) {
			this.setState({ screen: this.state.screen - 1 })
		}
	}

	handleConnect = () => {
		const { wifiName, wifiPass } = this.state
		apiPost(urlConnect, { ssid: wifiName, password: wifiPass })
			.then(r => {
				if (r.status === 'ok') this.props.finish(this.state.wifiName, this.state.wifiPass)
				else if (r.status === 'err_auth') {
					this.setState({ screen: 0 })
					alert('Contraseña incorrecta')
				} else {
					this.setState({ screen: 0 })
					alert('Error en el servidor')
				}
			})
			.catch(r => {
				alert('error en el servidor')
				this.setState({ screen: 0 })
			})
		this.setState({ open: false })
		this.nextScreen()
	}

	handleEnter = e => {
		const { wifiName, wifiPass, security } = this.state
		if (e.key === 'Enter')
			if (security === 'off' || (wifiName && wifiPass && security === 'on')) this.handleConnect()
	}

	renderDialog = () => {
		const { classes } = this.props
		const { wifiName, wifiPass, open, showPass, security, ssidManual } = this.state
		return (
			<Dialog open={open} className={classes.root}>
				<DialogTitle className={classes.dialogTitle}>
					<IconButton
						className={classes.backButton}
						size="small"
						onClick={() => this.setState({ open: false, wifiPass: '' })}
					>
						<ArrowBack />
					</IconButton>
					{ssidManual ? 'Conectar a red oculta' : wifiName}
				</DialogTitle>
				<DialogContent>
					{security === 'on' ? (
						<React.Fragment>
							<Typography variant="subtitle1" id="simple-modal-description">
								Ingrese {ssidManual ? 'el SSID y' : ''} la clave para conectarse a la red.
							</Typography>
							{ssidManual ? (
								<TextField
									id={'wifiName'}
									name={'wifiName'}
									key={'wifiName'}
									placeholder={'SSID'}
									className={classes.textField}
									value={wifiName}
									onChange={e => this.setState({ wifiName: e.target.value })}
									onKeyPress={this.handleEnter}
									margin="normal"
									variant="filled"
									fullWidth
									InputProps={{
										autoComplete: 'off',
										style: { width: '100%', height: '100%', borderRadius: 10 },
										disableUnderline: true
									}}
									inputProps={{
										style: { padding: '20px 0px 20px 10px' }
									}}
								/>
							) : null}
							<TextField
								id={'wifiPass'}
								name={'wifiPass'}
								key={'wifiPass'}
								placeholder={'Contraseña'}
								type={showPass ? 'text' : 'password'}
								className={classes.textField}
								value={wifiPass}
								onChange={e => this.setState({ wifiPass: e.target.value })}
								onKeyPress={this.handleEnter}
								margin="normal"
								variant="filled"
								fullWidth
								InputProps={{
									autoComplete: 'off',
									style: { width: '100%', height: '100%', borderRadius: 10 },
									disableUnderline: true,
									endAdornment: (
										<InputAdornment position="end">
											<IconButton
												aria-label="Toggle password visibility"
												onClick={() => this.setState({ showPass: !showPass })}
											>
												{!showPass ? <Visibility /> : <VisibilityOff />}
											</IconButton>
										</InputAdornment>
									)
								}}
								inputProps={{
									style: { padding: '20px 0px 20px 10px' }
								}}
							/>
						</React.Fragment>
					) : null}
				</DialogContent>
				<DialogActions className={classes.actions}>
					<Button
						className={classes.connButton}
						variant="contained"
						color="primary"
						onClick={this.handleConnect}
						disabled={!wifiName || (!wifiPass && security === 'on')}
					>
						Conectar
					</Button>
				</DialogActions>
			</Dialog>
		)
	}

	renderItem = (item, i) => {
		const { classes } = this.props
		const Quality =
			item.quality < 20
				? SignalWifi0Bar
				: item.quality < 40
					? SignalWifi1Bar
					: item.quality < 60 ? SignalWifi2Bar : item.quality < 80 ? SignalWifi3Bar : SignalWifi4Bar
		return (
			<ListItem
				key={'wifiitem-' + i}
				button
				onClick={() => {
					this.setState({ wifiName: item.essid, open: true, security: item.security, ssidManual: false })
				}}
			>
				<div className={classes.rootItem}>
					<div className={classes.primaryLineItem}>
						<ListItemText key={'wifiitemtext-' + i}>{item.essid}</ListItemText>
						{item.security === 'on' ? (
							<Lock key={'wifiitemsecicon-' + i} className={classes.iconsItem} />
						) : null}
						<Quality key={'wifiitemicon-' + i} className={classes.iconsItem} />
					</div>
				</div>
			</ListItem>
		)
	}

	addManual = () => {
		this.setState({ ssidManual: true, wifiName: '', open: true })
	}

	render() {
		const { classes } = this.props
		const { screen, wifiName, wifiList } = this.state
		return (
			<div className={classes.root}>
				{screen === 0 ? (
					<div className={classes.root}>
						<div className={classes.title}>
							<Typography style={{ color: '#707070' }} variant="h6" component="h6">
								SELECCIONA TU RED WIFI
							</Typography>
							<Typography style={{ color: '#707070' }}>
								Se necesita una red con acceso a internet para realizar la configuracion
							</Typography>
						</div>
						<div className={classes.content}>
							<Paper className={classes.paper}>
								{wifiList && wifiList.length > 0 ? (
									<List>{wifiList.map((item, index) => this.renderItem(item, index))}</List>
								) : (
									<Fade right>
										<Typography style={{ color: '#707070', paddingTop: 10, paddingLeft: 10 }}>
											Buscando redes...
										</Typography>
									</Fade>
								)}
							</Paper>
						</div>
						<Button className={classes.manualButton} variant="contained" color="primary" onClick={this.addManual}>
							Conectar a red oculta
						</Button>
					</div>
				) : screen === 1 ? (
					<div className={classes.root}>
						<div className={classes.title}>
							<Typography style={{ color: '#707070' }} variant="h6" component="h6">
								Conectando a la red {wifiName}...
							</Typography>
						</div>
						<div className={classes.content}>
							<Paper className={classes.waitingPaper}>
								<CircularProgress color="primary" />
							</Paper>
						</div>
					</div>
				) : null}
				{this.renderDialog()}
			</div>
		)
	}
}

export default withStyles(styles)(WifiList)
