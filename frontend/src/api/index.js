export const apiGet = (url) => fetch(url)
                            .then(v => v.json())
                            .then(data => data);

export const apiPut = (url, obj) => 
    fetch(`${url}`, {
    method: 'PUT',
    body: JSON.stringify(obj),
    headers: new Headers({ 'Content-type': 'application/json' })
    }).then(v => v.json())
    .then( r => {
        if (r.error) {
            return Promise.reject(r.validation);
        }
        return r;
    });

export const apiPost = (url, obj) => {
    return fetch(`${url}`, {
        method: 'POST',
        body: JSON.stringify(obj),
        headers: new Headers({ 'Content-type': 'application/json'})
    }).then(v => v.json())
    .then(r => {
        if (r.error) {
            return Promise.reject(r.validation);
        }
        return r;
    }).catch(r => {
        //console.log(r)
        return {status: 'err'}
    })
}

export const apiGetWithParams = (url, params) => {
    var toFetch = new URL(url)
    toFetch.search = new URLSearchParams(params)

    return(fetch(toFetch).then(r => r.json()).catch(r => ({status: 'err'})))
}
