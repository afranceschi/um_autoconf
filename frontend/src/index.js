import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { ThemeProvider } from '@material-ui/styles'
import { createMuiTheme } from '@material-ui/core'

const theme = createMuiTheme({
	typography: {
		useNextVariants: true,
		fontFamily: 'Gotham-Medium'
	},
	palette: {
		primary: { main: '#AC0038', contrastText: '#fff' },
		secondary: { main: '#F7F7F7', contrastText: '#fff' },
		navbar: 'white',
		sidebarTitle: '#333',
		sidebarContent: '#555',
		materialBorder: 'rgba(0, 0, 0, 0.23)'
	}
})
ReactDOM.render(
	<ThemeProvider theme={theme}>
		<App />
	</ThemeProvider>,
	document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
